package ru.mail.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Auxiliary class.
 * Contains auxiliary methods for working with UI elements.
 */
public class UiHelper {
    WebDriver driver;

    public UiHelper(WebDriver driver) {
        this.driver = driver;
    }

    private WebElement getElement(By locator) {
        return driver.findElement(locator);
    }


    public void clickElement(By locator) {
        waitVisiblEelement(locator);
        getElement(locator).click();
    }

    public void clearField(By locator) {
        getElement(locator).clear();
    }

    public void inputField(By locator, String inputText) {
        clearField(locator);
        getElement(locator).sendKeys(inputText);
    }

    public void waitVisiblEelement(By expectedElement) {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOf(getElement(expectedElement)));
    }

    public void assertion(String expectedValue, By locatorElement) {
        WebElement element = getElement(locatorElement);
        String actualValue = element.getText();
        Assert.assertEquals(expectedValue, actualValue);
    }

    /**
     * To go into the iFrame.
     * After completing the work close the iFrame - switchOffFrame()
     */
    public void switchToFrame(By frameLocator) {
        driver.switchTo().frame(getElement(frameLocator));
    }

    /**
     * Closes the iFrame after work
     */
    public void switchOffFrame() {
        driver.switchTo().defaultContent();
    }
}
