package ru.mail.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.mail.core.UiHelper;

/**
 * The implementation of the template PaigeObject.
 * CreateLetterPage - class describing letter creation page.
 * https://e.mail.ru/compose/?1544986934479
 */
public class CreateLetterPage {
    WebDriver driver;
    private UiHelper uiHelper;

    public static By writeLetterButton = By.xpath("//span[.='Написать письмо']");
    public static By toWhomField = By.xpath("//textarea[@*='To']");
    public static By subjectLetterField = By.cssSelector("input[name='Subject']");
    public static By iFrame = By.xpath("//iframe[contains(@id,'composeEditor_ifr')]");
    public static By bodyLetterField = By.xpath("//*[@id='tinymce']");
    public static By sendLetterButton = By.xpath("//*[contains(@title, 'Отправить')]");
    public static By textEmailSent = By.xpath("//*[@class='message-sent__title']");

    public CreateLetterPage(WebDriver driver) {
        this.driver = driver;
        uiHelper = new UiHelper(driver);
    }

    public CreateLetterPage clickWriteLetterButton() {
        uiHelper.clickElement(writeLetterButton);
        return this;
    }

    public CreateLetterPage enterFieldToWhom(String email) {
        uiHelper.inputField(toWhomField, email);
        return this;
    }

    public CreateLetterPage enterSubjectLetterField(String text) {
        uiHelper.inputField(subjectLetterField, text);
        return this;
    }

    public CreateLetterPage enterFieldBodyLetter(String text) {
        uiHelper.switchToFrame(iFrame);
        uiHelper.inputField(bodyLetterField, text);
        uiHelper.switchOffFrame();
        return this;
    }

    public CreateLetterPage clickSendLetterButton() {
        uiHelper.clickElement(sendLetterButton);
        return this;
    }
}
