package ru.mail.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import ru.mail.core.UiHelper;

/**
 * The implementation of the template PaigeObject.
 * MailLoginPage - class description of the authorization page in the mail.ru
 * https://mail.ru/
 */
public class MailLoginPage {
    WebDriver driver;
    private UiHelper uiHelper;

    public static By loginField = By.xpath("//*[@id='mailbox:login']");
    public static By passwordField = By.xpath("//*[@id='mailbox:password']");
    public static By submitButton = By.xpath("//*[@id='mailbox:submit']");

    public MailLoginPage(WebDriver driver) {
        this.driver = driver;
        uiHelper = new UiHelper(driver);
    }

    public MailLoginPage enterlogin(String login) {
        uiHelper.inputField(loginField, login);
        return this;
    }

    public MailLoginPage enterPassword(String password) {
        uiHelper.inputField(passwordField, password);
        return this;
    }

    public MailLoginPage clickLoginButton() {
        uiHelper.clickElement(submitButton);
        return this;
    }
}