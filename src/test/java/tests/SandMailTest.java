package tests;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.mail.core.Initialization;
import ru.mail.core.UiHelper;
import ru.mail.pageobject.CreateLetterPage;
import ru.mail.pageobject.MailLoginPage;

/**
 * Test verifying sending a letter
 */
public class SandMailTest extends Initialization {
    private UiHelper uiHelper;
    private MailLoginPage mailLoginPage;
    private CreateLetterPage createLetterPage;

    @BeforeTest
    public void precondition() throws Exception {
        uiHelper = new UiHelper(driver);
        mailLoginPage = new MailLoginPage(driver);
        createLetterPage = new CreateLetterPage(driver);
        driver.get("https://mail.ru/");
        uiHelper.waitVisiblEelement(MailLoginPage.loginField);

        mailLoginPage
                .enterlogin("ko-ko-ko-test@mail.ru")
                .enterPassword("etokokokotest123")
                .clickLoginButton();
    }

    @Test
    public void testSendMail() {
        uiHelper.waitVisiblEelement(CreateLetterPage.writeLetterButton);
        createLetterPage
                .clickWriteLetterButton()
                .enterFieldToWhom("yanteppe@gmail.com")
                .enterSubjectLetterField("Тест")
                .enterFieldBodyLetter("Тест тест тест")
                .clickSendLetterButton();
        uiHelper.waitVisiblEelement(CreateLetterPage.textEmailSent);
        uiHelper.assertion("Ваше письмо отправлено. Перейти во Входящие", CreateLetterPage.textEmailSent);
    }
}
